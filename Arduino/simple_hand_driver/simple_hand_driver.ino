#include "pyCommsLib.h"

int pinArray[2][9] =  
{
  {12, 10, 8, 6, 4, 13, 15, 17, 19},  
  {11, 9,  7, 5, 3, 14, 16, 18, 20}
};
int prevDir[9] = {0, 0, 0, 0, 0, 0, 0, 0, 0};

void setup() {
  // put your setup code here, to run once:

  for(int pin = 2; pin<22; pin++) {
    pinMode(pin, OUTPUT);
    digitalWrite(pin, LOW);
  }

  Serial.begin(115200);
  init_python_communication();
}

void loop() {
  if(latest_received_msg() != "NO_PYTHON_MESSAGE"){
    
    for(int i = 0; i<9; i++) {
      String temp = latest_received_msg(i);
      int dir = temp.toFloat();
      move_motor(i, dir);
    }
  }
  
  sync(); 
}

void move_motor(int id, int dir){

  int pinA = pinArray[0][id];
  int pinB = pinArray[1][id];

  if(dir == 1){
    digitalWrite(pinA, LOW);
    digitalWrite(pinB, HIGH);
    prevDir[id] = 1;
  }
  else if(dir == -1){
    digitalWrite(pinA, HIGH);
    digitalWrite(pinB, LOW);
    prevDir[id] = -1;
  }
  else{
    if(prevDir[id] == 1) {
      digitalWrite(pinA, HIGH);
      digitalWrite(pinB, LOW);
    }
    else if(prevDir[id] == -1) {
      digitalWrite(pinA, LOW);
      digitalWrite(pinB, HIGH);
    }
    delay(8);
    prevDir[id] = 0;
    digitalWrite(pinA, LOW);
    digitalWrite(pinB, LOW);
  }
}
