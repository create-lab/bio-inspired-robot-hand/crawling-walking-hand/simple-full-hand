from comms_wrapper import Arduino, Key

def main():
    arduino = Arduino("Hand motor controller Arduino", "/dev/ttyACM0", 115200)
    key = Key()

    arduino.connect_and_handshake()

    fwd_keys = ["1", "2", "3", "4", "5", "6", "7", "8", "9"]
    rev_keys = ["q", "w", "e", "r", "t", "y", "u", "i", "o"]
    while 1:
        motor_command = []
        for i in range(9):
            if key.keyPress == fwd_keys[i]:
                motor_command.append("1")
            elif key.keyPress == rev_keys[i]:
                motor_command.append("-1")
            else:
                motor_command.append("0")

        arduino.send_message(motor_command)

if __name__ == "__main__":
    main()